(function() {
    'use strict';
    angular.module('LifeBoat')
            .factory('toastService', toastService);

    function toastService() {
        return {
            notify: function(message, position) {
              if(window.plugins && window.plugins.toast){
                ///then execute ur code
                window.plugins.toast.showWithOptions(
                    {
                      message: message,
                      duration: "short",
                      position: position
                    }
                  );
                }else{
                // this means on browser simply alert the message
                alert(message);
                }
            }
        }
    };

})();