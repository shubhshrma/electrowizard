(function () {
  'use strict';
  angular.module('LifeBoat')
    .factory('userValidate', userValidate);

  function userValidate(localStorageService, $state, $stateParams, $rootScope, $location) {
    return {
      validUser: function () {
        $rootScope.$on('$stateChangeStart',
          function (event, toState, toParams, fromState, fromParams) {
            var userData = localStorageService.get('LoginData');
            if (userData) {
              if (toState.name == 'menu.dashboard' ||
                toState.name == 'menu.create_case' ||
                toState.name == 'menu.create_case.search_number' ||
                toState.name == 'menu.create_case.select_patients' ||
                toState.name == 'menu.create_case.create_case_form' ||
                toState.name == 'menu.create_case.select_address' ||
                toState.name == 'menu.create_case.assign_packages' ||
                toState.name == 'menu.create_case.create_address_form' ||
                toState.name == 'menu.create_case.collect_sample' ||
                toState.name == 'menu.create_case.assign_case' ||
                toState.name == 'menu.search_patient' ||
                toState.name == 'menu.transactions_console' ||
                toState.name == 'menu.search_patient.details' ||
                toState.name == 'menu.master_data' ||
                toState.name == 'menu.master_data.partner' ||
                toState.name == 'menu.master_data.package' ||
                toState.name == 'menu.master_data.partnerform' ||
                toState.name == 'menu.master_data.master_categories' ||
                toState.name == 'menu.master_data.masterCategoryForm' ||
                toState.name == 'menu.master_data.master_categories_value' ||
                toState.name == 'menu.master_data.master_categories_valueForm' ||
                toState.name == 'menu.master_data.packageForm'||
                toState.name == 'menu.master_data.package_test_parameters'||
                toState.name == 'menu.master_data.package_test_parametersForm'||
                toState.name == 'menu.master_data.partner_client'||
                toState.name == 'menu.master_data.partner_clientForm'
              ) {
                console.log(toState.name)
              } else {
                $state.transitionTo('menu.dashboard');
                event.preventDefault();
              }
            } else {
              if (toState.name == 'login') {
                console.log(toState.name);
              } else {
                $state.transitionTo("login");
                event.preventDefault();
              }
            }
          })
      }
    }
  }

})();
