(function () {
  'use strict';

  angular
    .module('LifeBoat')
    .service('ajaxService', ajaxService);

  function ajaxService($q, $http) {
    var service = {};

    service.send = function (api, data, method) {
      var def = $q.defer();
      $http({
        url:  api,
        method: method,
        data: data,
      }).success(function (res) {
        def.resolve(res);
      }).error(function () {
        $ionicLoading.hide();
      });
      return def.promise;
    }

    return service;
  }
})();
