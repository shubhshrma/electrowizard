(function () {
  'use strict';

  angular
    .module('LifeBoat')
    .service('Coordinates', Coordinates);

  function Coordinates(Configuration, $http, $q) {
    var service = {};

    service.Get = function (address) {
      var promise = $q.defer();
      var address_to_code = address.addressLabel+'+'+address.addressline1+'+'+address.addressline2+'+'+address.city+'+'+address.state;
      $http.get("https://maps.googleapis.com/maps/api/geocode/json?address=" + address_to_code + "&key=" + Configuration.GeoCodekey)
        .then(function (res) {
          address.latitude = res.data.results[0].geometry.location.lat;
          address.longitude = res.data.results[0].geometry.location.lng;
          promise.resolve(address);
        })
      return promise.promise;
    }

    return service;
  }
})();
