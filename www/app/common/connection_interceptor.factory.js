(function() {
    'use strict';
    angular
        .module('LifeBoat')
        .factory('connectionInterceptor', connectionInterceptor);

    function connectionInterceptor($q, $rootScope, toastService) {
        return {
            'requestError': function(rejection) {
                $rootScope.$broadcast('Http_rejection', rejection);
                 console.log('rejection', rejection)
            },
            'responseError': function(response) {

              console.log('response', response)
              toastService.notify(response.statusText, 'top')
                $rootScope.$broadcast('Http_rejection', response);
            }
        }
    }
})();
