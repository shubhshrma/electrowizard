(function() {
  'use strict'
  angular
    .module('LifeBoat')

    .filter('tinyToEnabledDisabled', function() {
      return function(input) {
        if (input == 1)
          return 'Enabled';
        else
          return 'Disabled';

      }
    })

    .filter('TinyIntToBoolean', function() {
      return function(input) {
        if (input == 1)
          return true;
        else
          return false;

      }
    })

    .filter('BooleanToTinyInt', function() {
      return function(input) {
        if (input == true)
          return 1;
        else
          return 0;

      }
    })

    .filter('time', function() {
      function calculateTime(isodate) {
        var time = new Date(isodate);
        var clock = time.toLocaleString('en-US', {
          hour: 'numeric',
          minute: 'numeric',
          hour12: true
        });
        var date = time.toDateString();
        return date + '@' + clock

      }

      return function(isodate) {
        return calculateTime(isodate);
      };
    })

})();