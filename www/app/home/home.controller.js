(function() {
  'use strict';

  angular
    .module('LifeBoat')
    .controller('homeCtrl', homeCtrl);

  function homeCtrl(localStorageService, $q, $http, $state) {
    var self = this;


    self.getDataResident = function() {
      var def = $q.defer();
      $http({
        url: "http://159.89.171.173:3000/api/Resident/" + localStorageService.get('residentID'),
        method: "GET",
        params: {},
      }).success(function(res) {
        def.resolve(res);
        console.log(res);
        self.data = res;
        localStorageService.set("userData", res);

      }).error(function() {
        $ionicLoading.hide();
      });

    }

    self.getDataCoins = function() {
      var def = $q.defer();
      $http({
        url: "http://159.89.171.173:3000/api/Coins/CO_" + localStorageService.get('residentID'),
        method: "GET",
        params: {},
      }).success(function(res) {
        def.resolve(res);
        self.coins = res;
      }).error(function() {
        $ionicLoading.hide();
      });


    }

    self.getDataEnergy = function() {
      var def = $q.defer();
      $http({
        url: "http://159.89.171.173:3000/api/Energy/EN_" + localStorageService.get('residentID'),
        method: "GET",
        params: {},
      }).success(function(res) {
        def.resolve(res);
        self.energy = res;
      }).error(function() {
        $ionicLoading.hide();
      });

    }

    self.getDataCash = function() {
      var def = $q.defer();
      $http({
        url: "http://159.89.171.173:3000/api/Cash/CA_" + localStorageService.get('residentID'),
        method: "GET",
        params: {},
      }).success(function(res) {
        def.resolve(res);
        self.cash = res;
      }).error(function() {
        $ionicLoading.hide();
      });

    }



    self.onInit = function() {
      console.log("hii");
      self.getDataResident();
      self.getDataCash();
      self.getDataEnergy();
      self.getDataCoins();
    }

    self.onInit();

    self.gotoTradeEnergy = function() {
      $state.go('app.tradeEnergy');
    }

    self.doRefresh = function() {
      self.onInit();
      $scope.$broadcast('scroll.refreshComplete');
      $scope.$apply();
    };

  }


})();