(function() {
  'use strict';

  angular
    .module('LifeBoat')
    .controller('tradeEnergyCtrl', tradeEnergyCtrl);


  function tradeEnergyCtrl(ajaxService, $scope, $q, $http, $state, localStorageService, $ionicPopup) {
    var self = this;
    self.energyRate = 1;
    self.searchForTraders = function() {
      var def = $q.defer();
      //neq
      $http({
        url: "http://159.89.171.173:3000/api/Energy",
        method: "GET",
        params: {
          "filter": {
            "where": {
              "and": [{
                  "value": {
                    "gt": self.power
                  }
                },
                {
                  "ownerID": {
                    "neq": localStorageService.get('residentID')
                  }
                }

              ]

            }
          }
        },
      }).success(function(res) {
        def.resolve(res);
        console.log(res)
        self.traders = res;
        console.log(res)
      }).error(function() {
        $ionicLoading.hide();
      });
    }

    self.buy = function(ownerID) {
      var def = $q.defer();
      console.log(ownerID);
      $http({
          url: "http://159.89.171.173:3000/api/Coins/CO_" + localStorageService.get('residentID'),
          method: "GET",
          params: {},
        })
        .then(function(res) {
          console.log(res.data.value);
          if (res.data.value >= self.energyRate * self.power) {

            $http({
                url: "http://159.89.171.173:3000/api/EnergyToCoins",
                method: "POST",
                data: {
                  "$class": "org.electrowizard.energy.network.EnergyToCoins",
                  "energyRate": self.energyRate,
                  "energyValue": self.power,
                  "coinsInc": "resource:org.electrowizard.energy.network.Coins#CO_" + ownerID,
                  "coinsDec": "resource:org.electrowizard.energy.network.Coins#CO_" + localStorageService.get('residentID'),
                  "energyInc": "resource:org.electrowizard.energy.network.Energy#EN_" + localStorageService.get('residentID'),
                  "energyDec": "resource:org.electrowizard.energy.network.Energy#EN_" + ownerID,
                }
              })
              .then(function(res) {

                $ionicPopup.alert({
                  title: 'Bought Energy!',
                });

                var data = {
                  "transactionId": res.data.transactionId,
                  "status": 0
                };
                var json = JSON.stringify(data);
                $state.go('app.single_transactions', {
                  data: json
                })

              })

          } else {
            console.log('insufficient coins')
          }



        })
    }



  }
})();