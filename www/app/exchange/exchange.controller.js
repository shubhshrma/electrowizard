(function() {
  'use strict';

  angular
    .module('LifeBoat')
    .controller('exchangeCtrl', exchangeCtrl);

  function exchangeCtrl(localStorageService, $q, $http, $ionicPopup, $state) {
    var self = this;
    self.cashRate = 10
    console.log("hi");
    self.flag = 0;
    self.flag1 = 0;
    self.showfield = function() {
      if (self.flag == 1)
        self.flag = 0;
      else
        self.flag = 1;
      self.flag1 = 0;
    }
    self.showfield1 = function() {
      if (self.flag1 == 1)
        self.flag1 = 0;
      else
        self.flag1 = 1;
      self.flag = 0;
    }

    self.exchangeCoinsTOCash = function() {

      $http({
          url: "http://159.89.171.173:3000/api/Coins/CO_" + localStorageService.get('residentID'),
          method: "GET",
          params: {}
        })
        .then(function(res) {

          console.log('current coins', res.data.value)
          if (res.data.value >= self.noOfCoins) {

            $http({
                url: "http://159.89.171.173:3000/api/Cash/CA_SBI",
                method: "GET",
                params: {}
              })
              .then(function(res) {
                console.log('current cash in bank', res.data.value);
                if (self.cashRate * self.noOfCoins < res.data.value) {
                  $http({
                      url: "http://159.89.171.173:3000/api/CashToCoins",
                      method: "POST",
                      data: {
                        "$class": "org.electrowizard.energy.network.CashToCoins",
                        "cashRate": self.cashRate,
                        "cashValue": self.noOfCoins,
                        "coinsInc": "resource:org.electrowizard.energy.network.Coins#CO_SBI",
                        "coinsDec": "resource:org.electrowizard.energy.network.Coins#CO_" + localStorageService.get('residentID'),
                        "cashInc": "resource:org.electrowizard.energy.network.Cash#CA_" + localStorageService.get('residentID'),
                        "cashDec": "resource:org.electrowizard.energy.network.Cash#CA_SBI",

                      }
                    })
                    .then(function(res) {
                      console.log(res);
                    })

                }

              })

          } else {
            console.log("insufficient Coins")
          }

        })

    }


    self.exchangeCashToCoins = function() {

      $http({
          url: "http://159.89.171.173:3000/api/Cash/CA_" + localStorageService.get('residentID'),
          method: "GET",
          params: {}
        })
        .then(function(res) {
          console.log(res.data.value)
          console.log(self.noOfCash)
          if (res.data.value >= self.noOfCash) {
            console.log(1);
            $http({
                url: "http://159.89.171.173:3000/api/Coins/CO_SBI",
                method: "GET",
                params: {}
              })
              .then(function(res) {
                console.log(res.data.value);
                if (self.noOfCash < res.data.value * self.cashRate) {
                  var date = new Date();
                  $http({
                      url: "http://159.89.171.173:3000/api/CashToCoins",
                      method: "POST",
                      data: {
                        "$class": "org.electrowizard.energy.network.CashToCoins",
                        "cashRate": self.cashRate,
                        "cashValue": self.noOfCash,
                        "coinsInc": "resource:org.electrowizard.energy.network.Coins#CO_" + localStorageService.get('residentID'),
                        "coinsDec": "resource:org.electrowizard.energy.network.Coins#CO_SBI",
                        "cashInc": "resource:org.electrowizard.energy.network.Cash#CA_SBI",
                        "cashDec": "resource:org.electrowizard.energy.network.Cash#CA_" + localStorageService.get('residentID'),
                        "timestamp": date.toISOString()
                      }
                    })
                    .then(function(res) {

                      $ionicPopup.alert({
                        title: 'Bought Cash!',
                      });

                      var data = {
                        "transactionId": res.data.transactionId,
                        "status": 0
                      };
                      var json = JSON.stringify(data);
                      $state.go('app.single_transactions', {
                        data: json
                      })
                      console.log(res);
                    })

                }

              })

          } else {
            console.log("insufficient Coins")
          }

        })

    }


  }

})();