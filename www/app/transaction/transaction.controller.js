(function() {
  'use strict';

  angular
    .module('LifeBoat')
    .controller('transactionCtrl', transactionCtrl);

  function transactionCtrl($http, ajaxService, $scope, $state, localStorageService) {
    var self = this;
    self.getEnergyToCoinsTransactions = function() {
      $http({
          url: "http://159.89.171.173:3000/api/EnergyToCoins",
          method: "GET",
          params: {
            "filter": {
              "where": {
                "or": [{
                    "coinsInc": "resource:org.electrowizard.energy.network.Coins#CO_" + localStorageService.get("residentID")
                  },
                  {
                    "coinsDec": "resource:org.electrowizard.energy.network.Coins#CO_" + localStorageService.get("residentID")
                  }
                ]
              }
            }
          }
        })
        .then(function(res) {
          self.info = res.data;
          console.log(res.data);
        })

    }

    self.getCashToCoinsTransactions = function() {
      $http({
          url: "http://159.89.171.173:3000/api/CashToCoins",
          method: "GET",
          params: {
            "filter": {
              "where": {
                "or": [{
                    "coinsInc": "resource:org.electrowizard.energy.network.Coins#CO_" + localStorageService.get("residentID")
                  },
                  {
                    "coinsDec": "resource:org.electrowizard.energy.network.Coins#CO_" + localStorageService.get("residentID")
                  }
                ]
              }
            }
          }
        })
        .then(function(res) {
          self.info2 = res.data;
          console.log(res.data);
        })

    }

    self.onInit = function() {
      self.getCashToCoinsTransactions();
      self.getEnergyToCoinsTransactions();
    }

    self.onInit();

    self.single1 = function(transid) {
      var data = {
        "transactionId": transid,
        "status": 0
      };
      var json = JSON.stringify(data);
      console.log(json);
      $state.go("app.single_transactions", {
        data: json
      });
    }

    self.single2 = function(transid) {
      var data = {
        "transactionId": transid,
        "status": 1
      };
      var json = JSON.stringify(data);
      console.log(json);
      $state.go("app.single_transactions", {
        data: json
      });
    }
    self.doRefresh = function() {
      self.onInit();
      $scope.$broadcast('scroll.refreshComplete');
      $scope.$apply();
    };

  }
})();