(function() {
  'use strict';

  angular.module('LifeBoat')
    .config(function($stateProvider, $urlRouterProvider, $httpProvider) {

      // $httpProvider.interceptors.push('connectionInterceptor');
      $stateProvider



        .state('login', {
          url: '/login',
          templateUrl: 'app/login/login.html',
          controller: 'LoginCtrl',
          controllerAs: 'Login'
        })
        .state('app', {
          url: '/app',
          abstract: true,
          templateUrl: 'app/menu/menu.html',
          controller: 'MenuCtrl',
          controllerAs: 'Menu'
        })

        .state('app.transactions', {
          url: '/transactions',
          views: {
            'menuContent': {
              templateUrl: 'app/transaction/transaction.html',
              controller: 'transactionCtrl',
              controllerAs: 'transaction'
            }
          }
        })
        .state('app.single_transactions', {
          url: '/single_transactions/:data',
          views: {
            'menuContent': {
              templateUrl: 'app/single_transaction/single_transaction.html',
              controller: 'singleTransactionCtrl',
              controllerAs: 'singleTransaction'
            }
          }
        })

        .state('app.search_patient', {
          url: '/search_patient',
          views: {
            'menuContent': {
              templateUrl: 'app/search_patient/search_patient.html',
              controller: 'search_patientCtrl',
              controllerAs: 'search_patient'
            }
          }
        })

        .state('app.home', {
          url: '/home',
          views: {
            'menuContent': {
              templateUrl: 'app/home/home.html',
              controller: 'homeCtrl',
              controllerAs: 'home'
            }
          }
        })


        .state('app.tradeEnergy', {
          url: '/tradeEnergy',
          views: {
            'menuContent': {
              templateUrl: 'app/tradeEnergy/tradeEnergy.html',
              controller: 'tradeEnergyCtrl',
              controllerAs: 'tradeEnergy'
            }
          }
        })

        .state('app.exchange', {
          url: '/exchange',
          views: {
            'menuContent': {
              templateUrl: 'app/exchange/exchange.html',
              controller: 'exchangeCtrl',
              controllerAs: 'exchange'
            }
          }
        })

      // .state('app.browse', {
      //     url: '/browse',
      //     views: {
      //       'menuContent': {
      //         templateUrl: 'templates/browse.html'
      //       }
      //     }
      //   })
      //   .state('app.playlists', {
      //     url: '/playlists',
      //     views: {
      //       'menuContent': {
      //         templateUrl: 'templates/playlists.html',
      //         controller: 'PlaylistsCtrl'
      //       }
      //     }
      //   })

      // .state('app.single', {
      //   url: '/playlists/:playlistId',
      //   views: {
      //     'menuContent': {
      //       templateUrl: 'templates/playlist.html',
      //       controller: 'PlaylistCtrl'
      //     }
      //   }
      // });
      // if none of the above states are matched, use this as the fallback
      // $urlRouterProvider.otherwise('login');
      $urlRouterProvider.otherwise('login');

    });
})();