(function() {
    'use strict';
    angular.module('LifeBoat', [
        'ionic',
        'ui.router',
        'ngStorage',
        'ion-floating-menu'
    ]);
})();