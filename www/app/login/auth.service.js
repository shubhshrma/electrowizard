(function() {
    'use strict';
    angular
        .module('LifeBoat')
        .factory('authService', authService);

    function authService(PartnerUsers, $rootScope) {

        var service = {
            login: login,
            logout: logout,
            register: register,
            isAuthenticated: isAuthenticated,
            getCurrentUser: getCurrentUser
        };
        return service;

        function login(username, password) {
            return PartnerUsers
                .login({
                    username: username,
                    password: password
                })
                .$promise;
        }

        function logout() {
            return PartnerUsers
                .logout()
                .$promise;
        }

        function register(username, password) {
            return PartnerUsers
                .create({
                    "partnerId": 1,
                    "username": "test1",
                    "firstName": 'test1',
                    "lastName": "test2",
                    "profileId": 1,
                    "designationId": 1,
                    "mobilityFlag": 'N',
                    "lastLoginDate": "2017-09-17T10:57:47.232Z",
                    "passwordResetFlag": '0',
                    "created_by": 1,
                    "last_modified_by": 1,
                    "primaryContact": 830370003,
                    "email": "test2@gmail.com",
                    // "emailVerified": true,
                    "password": "test"
                })
                .$promise;
        }

        function isAuthenticated() {
            return PartnerUsers.isAuthenticated();
        }

        function getCurrentUser() {
            return PartnerUsers.getCurrent();
        }

    }

})();
