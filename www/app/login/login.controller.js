(function () {
  'use strict';

  angular
    .module('LifeBoat')
    .controller('LoginCtrl', LoginCtrl);

  function LoginCtrl($state,  ajaxService, localStorageService) {
    var self = this;
    self.login = function () {
          localStorageService.set('residentID',self.residentID);
          $state.go('app.home') 
    }
  }
})();
