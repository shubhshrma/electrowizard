(function() {
    'use strict';

    angular
        .module('LifeBoat')
        .controller('MenuCtrl', MenuCtrl);

    function MenuCtrl(localStorageService, $state) {
        var self = this;
        
        self.logout = function(){
            localStorageService.clearAll();
            $state.go('login')
            console.log("Hi");
        }
         self.exchange = function(){
            $state.go('app.exchange');
            console.log("Hi");
        }
        self.home = function(){
            $state.go('app.home');
            console.log("Hi");
        }

        self.transactions = function(){
            $state.go('app.transactions');
            console.log("Hi");
        }
     
    }
})();