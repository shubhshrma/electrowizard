(function() {
  'use strict';

  angular
    .module('LifeBoat')
    .controller('singleTransactionCtrl', singleTransactionCtrl);


  function singleTransactionCtrl($stateParams, ajaxService, $scope, $q, $http, $state, localStorageService, $ionicPopup) {
    var self = this;
    var json = $stateParams.data;

    self.data = JSON.parse(json);

    console.log(self.data.status);

    self.getSingleTransactions1 = function() {
      $http({
          url: "http://159.89.171.173:3000/api/EnergyToCoins",
          method: "GET",
          params: {
            "filter": {
              "where": {
                "transactionId": self.data.transactionId
              }
            }
          }
        })
        .then(function(res) {
          if (res.data[0].coinsDec.split("_")[1] == localStorageService.get("residentID")) {
            self.TransactionType = "Coins To Energy";
            self.energyStatement = res.data[0].energyValue + " KWh Purchased";
            self.coinsStatement = res.data[0].energyValue / res.data[0].energyRate +"Paid" ;
            self.timestamp = (new Date(res.data[0].timestamp)).toDateString() + " at " + (new Date(res.data[0].timestamp)).toLocaleTimeString();
          } else {
            self.TransactionType = "Energy To Coins";
            self.energyStatement = res.data[0].energyValue + " KWh Sold";
            self.coinsStatement = res.data[0].energyValue / res.data[0].energyRate + " Earned";
            self.timestamp = (new Date(res.data[0].timestamp)).toDateString() + " at " + (new Date(res.data[0].timestamp)).toLocaleTimeString();
          }

        })

    }

    self.getSingleTransactions2 = function() {
      console.log(self.data.transactionId);
      $http({
          url: "http://159.89.171.173:3000/api/CashToCoins",
          method: "GET",
          params: {
            "filter": {
              "where": {
                "transactionId": self.data.transactionId
              }
            }
          }
        })
        .then(function(res) {
          console.log(res.data[0].cashValue);

          if (res.data[0].coinsDec.split("_")[1] == localStorageService.get("residentID")) {
            self.TransactionType = "Coins To Cash";
            self.CashStatement = "Got :" + res.data[0].cashValue + "";
            console.log(self.CashStatement);
            self.coinsStatement = "Given :" + res.data[0].cashValue / res.data[0].cashRate;
            self.timestamp = (new Date(res.data[0].timestamp)).toDateString() + " at " + (new Date(res.data[0].timestamp)).toLocaleTimeString();
          } else {
            self.TransactionType = "Cash To Coins";
            self.CashStatement = "Given :" + res.data[0].cashValue + "";
            console.log(self.CashStatement);
            self.coinsStatement = "Got :" + res.data[0].cashValue / res.data[0].cashRate;
            self.timestamp = (new Date(res.data[0].timestamp)).toDateString() + " at " + (new Date(res.data[0].timestamp)).toLocaleTimeString();
          }

        })

    }

    if (self.data.status == 0) {
      self.getSingleTransactions1();
    } else {
      self.getSingleTransactions2();
    }







  }
})();